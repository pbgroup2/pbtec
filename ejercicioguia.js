const stores = {
  d1: {
    papitasBQ: 3500,
    papitasLimon: 3200,
    mani: 800,
    cafe: 8000,
    azucar: 3500,
    papel: 3000,
  },
  olimpica: {
    papitasBQ: 3600,
    papitasPollo: 3500,
    mani: 850,
    cafe: 8010,
    azucar: 3200,
    papel: 3500,
  },
  exito: {
    papitasBQ: 3650,
    papitasLimon: 3200,
    mani: 790,
    cafe: 8000,
    azucar: 3500,
    papel: 3000,
    gomitas: 4520,
  },
  otra: {
    papitasBQ: 3700,
    papitasLimon: 3200,
    cafe: 7000,
    azucar: 3000,
    papel: 3200,
    gomitas: 4320,
  },
  jumbo: {
    papitasBQ: 3700,
    papitasPollo: 3200,
    cafe: 7000,
    azucar: 3000,
    mani: 3200,
    gomitas: 4320,
  },
  isimo: {
    papitasBQ: 3700,
    papitasPollo: 3200,
    cafe: 800,
    azucar: 3000,
    mani: 1800,
    gomitas: 4320,
  },
  carulla: {
    papitasBQ: 3700,
    papitasPollo: 3200,
    cafe: 800,
    azucar: 3000,
    mani: 500,
    gomitas: 4320,
  }
};

const shoppingList = ['papitasPollo', 'papitasBQ', 'cafe', 'gomitas', 'mani'];

const resumen = {
  complete: [],
  resume: {

  },
  mayor: {
  },
  menor: {
  }
}

let store = [];
let completeStores = false;

//valida que exista la propiedad
function validate(Lista) {
  let mayor = 0
  let menor = 0
  let storeMayor = ''
  let storeMenor = ''
  Object.entries(stores).forEach(([nameStore, products]) => {
    const value = Object.keys(products)
    const productsAll = value.filter(item => shoppingList.includes(item))
    let total = 0
    Object.entries(products).forEach(([productD, price]) => {
      const arr = []
      const search = productsAll.find(item => item === productD)
      search ? total = total + price[arr] : ''
      console.log(total)
    })
    // console.log(total)
    if (shoppingList.length === productsAll.length) {
      if (Lista.hasOwnProperty('complete')) {

        const max = Math.max(total)
        storeMayor = nameStore
        const min = Math.min(total)

        Lista.complete.push(nameStore)
        Lista.resume[nameStore] = {
          all: true,
          products: [...productsAll],
          total
        }
        Lista.mayor = {name: storeMayor, price: max}
        Lista.menor = {name: storeMenor, price: min}
      }
    } else {
      Lista.resume[nameStore] = {
        all: false,
        products: [...productsAll],
        total
      };
    }
  })
}

validate(resumen)
console.log(resumen)  
