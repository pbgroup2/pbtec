/* Realizar el registro de las bases de datos, con los datos suministrados, todo debe ser de manera dinamica.*/


/*
const nombresArea = ['Dirección', 'Contabilidad', 'Recursos Humanos', 'Tecnologia']
const nombrescargos = ['Gerente', 'Supervisor','Gerente Operativo', 'Gerente comercial', 'Backend', 'Frontend', 'Diseñador', 'Analista de datos',
'Contador', 'Aux. contable', 'Cajero', 'Tesoreria', 'Psicologo', 'RRH', 'Talento humano']


RELACION DE AREA CON CARGOS
Direccion: [Gerente, Supervisor, Gerente Operativo, Gerente comercial]
Contabilidad: [Contador, Aux. contable, Cajero, Tesoreria]
Recursos Humanos: [Psicologo, RRH, Talento humano]
Tecnologia: [Backend, Frontend, Diseñador, Analista de datos]

*/
const personas = [
    {nombre: "Pedro", cargo: "Gerente", cedula:159},
    {nombre: "Karla", cargo: "Supervisor", cedula:753},
    {nombre: "Sandra", cargo: "Gerente Operativo", cedula:456},
    {nombre: "Carlos", cargo: "Gerente comercial", cedula:654},
    {nombre: "Jesus", cargo: "Diseñador", cedula:951},
    {nombre: "Juan", cargo: "Backend", cedula:357},
    {nombre: "Andres", cargo: "Frontend", cedula:852},
    {nombre: "Teresa", cargo: "Backend", cedula:258},
    {nombre: "Sandro", cargo: "Frontend", cedula:123},
    {nombre: "Diomedes", cargo: "Anlista de datos", cedula:321},
    {nombre: "Camilo", cargo: "Contador", cedula:789},
    {nombre: "Marta", cargo: "Aux. contable", cedula:987},
    {nombre: "Rosario", cargo: "Cajero", cedula:758},
    {nombre: "Laura", cargo: "Tesoreria", cedula:857},
    {nombre: "Elver", cargo: "Psicologo", cedula:589},
    {nombre: "Duvan", cargo: "RRH", cedula:985},
    {nombre: "Gonzalo", cargo: "Talento humano", cedula:452},
    {nombre: "Pablo", cargo: "Frontend", cedula:254},
    {nombre: "Rodrigo", cargo: "Anlista de datos", cedula:365},
    {nombre: "Rafael", cargo: "Contador", cedula:563},
    {nombre: "Mario", cargo: "Aux. contable", cedula:965},
    {nombre: "Luigi", cargo: "Cajero", cedula:569},
    {nombre: "Kratoz", cargo: "Tesoreria", cedula:419},
    {nombre: "Beatriz", cargo: "Psicologo", cedula:914},
    {nombre: "Orlando", cargo: "RRH", cedula:752},
    {nombre: "Luis", cargo: "Talento humano", cedula:257}, 
]

/*
Guardar por ID, los ID deben ser generados de manera automatica y secuencial (auto-incrmentable)
Debe crear objetos para todo, ejemplo:
Area:[
    {id: 1, area: "Nombre del area"}
]

Cargos:[
    {id:1, cargo: "Nombre del cargo", idArea: 1}
]


Personas: [
    {nombre: "nombre de la persona", cedula="cedula de persona" idCargo: 1}
]
*/

// Estructura de la EMPRESA
/* 
La KEY principal va a ser el nombre del empleado
Empresa={
    "nombre del empleado":{
        cargo: "nombre del cargo"
        Area: "nombre del area"
    }
}
empresa debe inicializarse de esta manera, vacia
empresa = {}
*/

const relaciones = {
    "Direccion": ["Gerente", "Supervisor", "Gerente Operativo", "Gerente comercial"],
    "Contabilidad": ["Contador", "Aux. contable", "Cajero", "Tesoreria"],
    "Recursos Humanos": ["Psicologo", "RRH", "Talento humano"],
    "Tecnologia": ["Backend", "Frontend", "Diseñador", "Anlista de datos"]
}

const nombresArea = ['Dirección', 'Contabilidad', 'Recursos Humanos', 'Tecnologia'];
const nombresCargos = ['Gerente', 'Supervisor', 'Gerente Operativo', 'Gerente comercial', 'Backend', 'Frontend', 'Diseñador', 'Anlista de datos', 'Contador', 'Aux. contable', 'Cajero', 'Tesoreria', 'Psicologo', 'RRH', 'Talento humano'];


const empresa = {};
const areas = [];
const cargos = [];
const personasRegistradas = [];

const generateId = (array) => (array.length > 0 ? Math.max(...array.map(item => item.id)) + 1 : 1);

const registrarArea = (nombreArea) => {
    const id = generateId(areas);
    const nuevaArea = { id, area: nombreArea };
    areas.push(nuevaArea);
    return nuevaArea;
}

const registrarCargo = (nombreCargo, idArea) => {
    const id = generateId(cargos);
    const nuevoCargo = { id, cargo: nombreCargo, idArea };
    cargos.push(nuevoCargo);
    return nuevoCargo;
}

const registrarPersona = (nombrePersona, cedula, nombreCargo) => {
    const id = generateId(personasRegistradas);
    const cargoEncontrado = cargos.find(cargo => cargo.cargo === nombreCargo);
    const areaEncontrada = areas.find(area => area.id === cargoEncontrado.idArea);

    const nuevaPersona = { id, nombre: nombrePersona, cedula, idCargo: cargoEncontrado.id };
    empresa[nombrePersona] = { cedula, cargo: cargoEncontrado.cargo, area: areaEncontrada.id };
    personasRegistradas.push(nuevaPersona);
}

const relacionarNombres = () => {
    Object.keys(relaciones).forEach((nombreArea, index) => {
        const idArea = index + 1;
        registrarArea(nombreArea);

        const cargosEnArea = relaciones[nombreArea];
        cargosEnArea.forEach((nombreCargo) => registrarCargo(nombreCargo, idArea));
    });
}

relacionarNombres(nombresArea, nombresCargos);

personas.forEach(persona => registrarPersona(persona.nombre, persona.cedula, persona.cargo));

// console.log(areas);
// console.log(cargos)
console.log("Empresa:", empresa);





