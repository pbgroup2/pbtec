const stores = {
    d1: {
        papitasBQ: 3500,
        papitasLimon: 3200,
        mani: 800,
        cafe: 8000,
        azucar: 3500,
        papel: 3000,
    },
    olimpica: {
        papitasBQ: 3600,
        papitasPollo: 3500,
        mani: 850,
        cafe: 8010,
        azucar: 3200,
        papel: 3500,
    },
    exito: {
        papitasBQ: 3650,
        papitasPollo: 3200,
        mani: 790,
        cafe: 8000,
        azucar: 3500,
        papel: 3000,
        gomitas: 4520,
    },
    otra: {
        papitasBQ: 3700,
        papitasLimon: 3200,
        cafe: 7000,
        azucar: 3000,
        papel: 3200,
        gomitas: 4320,
    },
};

const shoppingList = ['papitasPollo', 'papitasBQ', 'cafe', 'gomitas', 'mani'];

// verificar si una tienda tiene todos los productos de la lista
const tieneTodosLosProductos = (tienda) => shoppingList.forEach(producto => tienda.hasOwnProperty(producto));

const products = tieneTodosLosProductos(stores.d1)
console.log(products)

// console.log(Object.values(stores))

// calcular el precio de la compra en una tienda
const calcularPrecioCompra = (tienda) => shoppingList.reduce((total, producto) => total + tienda[producto], 0);
console.log(calcularPrecioCompra(products))

// Encontrar tiendas disponibles
const tiendasDisponibles = Object.keys(stores).reduce((result, tiendaNombre) => {
    const tienda = stores[tiendaNombre];
    const tieneProductos = tieneTodosLosProductos(tienda);

    if (tieneProductos) {
        result[tiendaNombre] = {
            tieneProductos: true,
            ProductosDiponibles: shoppingList.filter(producto => tienda.hasOwnProperty(producto)),
            precioCompra: calcularPrecioCompra(tienda)
        };
    } else {
        result[tiendaNombre] = {
            tieneProductos: false,
            ProductosDiponibles: shoppingList.filter(producto => tienda.hasOwnProperty(producto)),
            precioCompra: calcularPrecioCompra(tienda)
        };
    }

    return result;
}, {});

// Encontrar el menor y mayor valor de la compra
const { menorCompra, mayorCompra } = Object.keys(tiendasDisponibles).reduce((result, tiendaNombre) => {
    const tienda = tiendasDisponibles[tiendaNombre];

    if (tienda.tieneProductos) {
        const precioCompra = tienda.precioCompra;

        if (!result.menorCompra || precioCompra < result.menorCompra.precioCompra) {
            result.menorCompra = {
                tienda: tiendaNombre,
                precioCompra,
            };
        }

        if (!result.mayorCompra || precioCompra > result.mayorCompra.precioCompra) {
            result.mayorCompra = {
                tienda: tiendaNombre,
                precioCompra,
            };
        }
    }

    return result;
}, { menorCompra: null, mayorCompra: null });

console.log("Tiendas disponibles:");
console.log(tiendasDisponibles);

if (menorCompra) {
    console.log("Menor compra:");
    console.log(menorCompra);
}

if (mayorCompra) {
    console.log("Mayor compra:");
    console.log(mayorCompra);
}
