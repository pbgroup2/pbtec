const students = [
    { id: 1, name: "Juan", age: 17, height: 1.60, gender: 'M' },
    { id: 4, name: "Pedro", age: 15, height: 1.90, gender: 'M' },
    { id: 2, name: "Ana", age: 16, height: 1.65, gender: 'F' },
    { id: 3, name: "Sofia", age: 17, height: 1.72, gender: 'F' },
    { id: 6, name: "Pedro", age: 12, height: 1.70, gender: 'M' },
    { id: 5, name: "Rosa", age: 19, height: 1.67, gender: 'F' },
];

const califications = [
    { id: 1, calification: 5 },
    { id: 4, calification: 5 },
    { id: 3, calification: 5 },
    { id: 2, calification: 1 },
    { id: 5, calification: 3 },
    { id: 6, calification: 4 },
]

const newStudents = [
    { name: "Hector", age: 17, height: 1.60, gender: 'M' },
    { name: "Juliana", age: 15, height: 1.90, gender: 'F' },
    { name: "Ashley", age: 19, height: 1.65, gender: 'F' },
    { name: "Valentina", age: 21, height: 1.65, gender: 'F' }
]

const newCalifications = [
    { id: 9, calification: 3 },
    { id: 7, calification: 4 },
    { id: 8, calification: 5 },
    { id: 10, calification: 1 },
]

const duplicateStudents = [...students]
const duplicateCalifications = [...califications]

const generateId = () => {
    const ids = students.map(element => element.id)
    return Math.max(...ids);
}

const addStudents = (newStudentList) => {
    let newId = generateId()
    const studentIds = newStudentList.map(student => ({ id: newId += 1, ...student }))
    duplicateStudents.push( ...studentIds )
}

addStudents(newStudents);
// console.log(duplicateStudents);

const addCalifications = (newCalificationsList) => {
    newCalificationsList.forEach(newCalification => {
        if(!califications.find(calification => calification.id === newCalification.id)) {
            duplicateCalifications.push( newCalification )
        } else {
            console.log(`El estudiante con el ID ${newCalification.id} ya existe`)
        }
    })
}

addCalifications(newCalifications);
// console.log(duplicateCalifications);

// ----------------------------------------- EXERCISE 3 ------------------------------------------- //

const updatedStudents = [];

const qualificationsShow = (totalStudents, qualifications) => {

    totalStudents.forEach((student) => {
    const { calification } = qualifications.find((cal) => cal.id === student.id);
    const updated = { ...student, calification };
    updatedStudents.push(updated);
  })
}

qualificationsShow(duplicateStudents, duplicateCalifications);
// console.log(updatedStudents);

const filterToGender = (genderList) => {
    const maleGender = genderList.filter((person) => person.gender == 'M').length
    const femaleGender = genderList.filter((person) => person.gender == 'F').length

    return [ femaleGender, maleGender ]
}

studentsCalificated = []
totalStudents = []
totalCalifications = []

// console.log(studentsCalificated);

// ----------------------------------------- FUNCTION STATE --------------------------------------- //

state = {}

const addGlobalState = (prop1, prop2, prop3, prop4, studentsData) => {

    studentsData.forEach((data) => {
        studentsCalificated.push({ id: data.id, name: data.name })
        totalStudents.push({ id: data.id, note: data.calification })
        totalCalifications.push({ id: data.id, name: data.name, age: data.age, height: data.height, gender: data.gender })
    })

    state[prop1] = [...studentsCalificated]
    state[prop2] = [ ...totalStudents ]
    state[prop3] = [ ...totalCalifications ]

    const filter = filterToGender(studentsData);
    state[prop4] = filter[0]
}

addGlobalState('calificated', 'califications', 'students', 'gender', updatedStudents);

// add('saludo')

console.log(state)



